var mODELcLASS = require('mODELcLASS');
var eNUMERATION = require('eNUMERATION');

var Genre = new eNUMERATION('Genre', [
  'Drama',
  'Documentary',
  'Fiction',
  'Comedy'
])

module.exports = new mODELcLASS({
  name: 'Book',
  properties: {
    isbn: {
      label: 'ISBN',
      range: 'NonEmptyString',
      isStandardId: true,
      pattern: /\b\d{9}(\d|X)\b/,
      patternMessage: 'The ISBN must be a 10-digit string or a 9-digit string followed by "X"!'
    },
    title: {
      label: 'Title',
      range: 'NonEmptyString',
      min: 2,
      max: 50
    },
    year: {
      label: 'Year',
      range: 'Integer',
      min: 1459,
      max: (new Date()).getFullYear()
    },
    edition: {
      label: 'Edition',
      range: 'PositiveInteger',
      optional: true
    },
    genre: {
      label: 'Genre',
      range: Genre,
      optional: true
    }
  }
});